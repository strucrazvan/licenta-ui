import React from 'react';
import {Alert, Button, TextInput, View, StyleSheet, Text,TouchableOpacity} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import {Header, Input, SearchBar,} from 'react-native-elements';
import {Collapse, CollapseHeader, CollapseBody} from "accordion-collapse-react-native";
import {Spinner, ListItem, Separator} from 'native-base';
import {Font} from 'expo';
import * as SQLite from 'expo-sqlite';
import NetInfo from "@react-native-community/netinfo";
import {stringify} from "qs";
import * as axios from 'axios';
import {navigate} from "react-navigation";
import Triangle from 'react-native-triangle';

const db = SQLite.openDatabase('shop.db')
var radio_props = [
    {label: 'User', value: 1},
    {label: 'Owner', value: 0}
];

export default class RestaurantLogin extends React.Component {
    state = {
        api: 'http://192.168.0.147:8000',
        username: '',
        password: '',
    };

    loginRestaurant(username, password) {
        let data = new URLSearchParams();
        data.append("username", username);
        data.append("password", password);
        const url = this.state.api + '/restaurant/login';
        axios.post(url, data)
            .then(
                res => {
                    console.log(res.data);
                    this.props.navigation.navigate('RestaurantHomeScreen',{username: username, password: password, restaurant: res.data});
                }
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                    else {
                        Alert.alert("Server error, please contact our team.");
                    }
                }
            });
    }
    loginHandler() {
        const {api, username, password} = this.state;
        if (username === '' || password === '') {
            Alert.alert("Please complete all the fields");
        }
        else {
            this.loginRestaurant(username,password);
        }
    }
    handleHomeButton() {
        this.props.navigation.navigate('Home');
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.header}>
                    Restaurant
                </Text>
                <Text>{this.state.emailError}</Text>

                <TextInput
                    onChangeText={(val) => this.setState({username: val})} value={this.state.username}

                    placeholder={'Username'}
                    style={styles.input}
                />
                <TextInput
                    onChangeText={(val) => this.setState({password: val})} value={this.state.password}
                    placeholder={'Password'}
                    secureTextEntry={true}
                    style={styles.input}
                />

                <TouchableOpacity
                    style={styles.loginScreenButton}
                    onPress={() =>this.loginHandler()}
                    underlayColor='#fff'>
                    <Text style={styles.loginText}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.triangleContainerRight}
                                   onPress={this.handleHomeButton.bind(this)}
                >
                    <Triangle
                        width={50}
                        height={50}
                        color={'#cc0000'}
                        direction={'left'}
                    />
                </TouchableOpacity >


            </View>
        );

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    input: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor:'#000'
    },
    header: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 60,
    },
    triangleContainerRight:{
        position: 'absolute',
        top: 40,
        left:10
    },
    loginScreenButton:{
        marginTop:10,
        marginLeft:0,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#000',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 0,
        borderColor:'#fff',
        width: 100,
    },
    loginText:{
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold',

    },
});
