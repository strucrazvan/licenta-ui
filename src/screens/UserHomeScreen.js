import React from 'react';
import {Alert, ScrollView, Button, TextInput, View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import {Header, Input, Overlay, SearchBar,} from 'react-native-elements';
import {Collapse, CollapseHeader, CollapseBody} from "accordion-collapse-react-native";
import {Spinner, ListItem, Separator} from 'native-base';
import {Font} from 'expo';
import * as SQLite from 'expo-sqlite';
import NetInfo from "@react-native-community/netinfo";
import {parse, stringify} from "qs";
import * as axios from 'axios';
import RNPickerSelect from 'react-native-picker-select';
import IconF from 'react-native-vector-icons/FontAwesome';
import DatePicker from 'react-native-datepicker'
import MapView from "react-native-maps/lib/components/MapView";
import {Marker} from 'react-native-maps';
import Geocode from "react-geocode";
import Autocomplete from "react-native-autocomplete-input";


import Triangle from 'react-native-triangle';



import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const db = SQLite.openDatabase('shop.db')
var radio_props = [
    {label: 'User', value: 1},
    {label: 'Owner', value: 0}
];

const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = 0.01;

const initialRegion = {
    latitude: -37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
}
export default class UserHomeScreen extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        console.log(props.username);
        console.log(props.state);

    }

    map = null;

    state = {
        api: 'http://192.168.0.147:8000',
        region: {
            latitude: -37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        ready: true,
        filteredMarkers: [],
        markers: [],
        restaurantsList: [],
        showRestaurant: false,
        restaurantName: '',
        restaurantDescription: '',
        restaurantAddress: '',
        restaurantPhoneNumber: '',
        restaurantFreeTables: '',
        restaurantTotalTables: '',
        restaurantType: '',
        restaurantNames: [],
        query: "",
        showSearchBar: false
    };


    getRestaurants() {
        let data = new URLSearchParams();

        data.append("username", this.props.navigation.state.params.username);
        data.append("password", this.props.navigation.state.params.password);

        axios.get(this.state.api + '/restaurant/getAll', data)
            .then(
                res => {
                    // console.log(res.data);
                    console.log("NEW REQUEST\n\n\n\n\n\n");
                    data = res.data;
                    var markers = this.state.markers;
                    data.forEach(function (entry) {
                        console.log(entry.address);
                        markers.push({
                            latitude: entry.address.latitude,
                            longitude: entry.address.longitude,
                            title: entry.name,
                            description: entry.description,
                            key: entry.id
                        });
                        // console.log(entry.latitude+"  "+entry.longitude);
                    });
                    console.log(markers);
                    this.setState({restaurantsList: res.data});
                    this.setState({markers: markers});
                }
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                } else {
                    Alert.alert("Server error, please contact our team.");
                }

            });
    }

    setRegion(region) {
        if (this.state.ready) {
            setTimeout(() => this.map.animateToRegion(region), 10);
        }
        //this.setState({ region });
    }

    componentDidMount() {
        this.getRestaurants();
        console.log('Component did mount');

        console.log(this.state.markers);
        this.getCurrentPosition();

    }

    getCurrentPosition() {
        try {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const region = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    };
                    this.setRegion(region);
                },
                (error) => {
                    //TODO: better design
                    switch (error.code) {
                        case 1:
                            if (Platform.OS === "ios") {
                                Alert.alert("", "To locate your location enable permission for the application in Settings - Privacy - Location");
                            } else {
                                Alert.alert("", "To locate your location enable permission for the application in Settings - Apps - ExampleApp - Location");
                            }
                            break;
                        default:
                            Alert.alert("", "Error detecting your location");
                    }
                }
            );
        } catch (e) {
            alert(e.message || "");
        }
    };

    onMapReady = (e) => {
        if (!this.state.ready) {
            this.setState({ready: true});
        }
    };

    onRegionChange = (region) => {
        // console.log('onRegionChange', region);
    };

    onRegionChangeComplete = (region) => {
        console.log(this.state.markers);
        // console.log('onRegionChangeComplete', region);
    };

    showMarker(key) {
        var restaurant = this.search(key, this.state.restaurantsList);
        this.setState({
            showRestaurant: true,
            restaurantName: restaurant.name,
            restaurantDescription: restaurant.description,
            restaurantAddress: restaurant.address,
            restaurantPhoneNumber: restaurant.phoneNumber,
            restaurantFreeTables: restaurant.freeTables,
            restaurantTotalTables: restaurant.totalTables,
            restaurantType: this.renderType(restaurant.type)
        });
    }
    showRestaurantSearch(restaurant) {
        const region = {
            latitude: restaurant.address.latitude,
            longitude: restaurant.address.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        };
        this.setRegion(region);

        setTimeout(() => this.map.animateToRegion(region), 10);
        this.setState({showSearchBar: !this.state.showSearchBar})
        // this.setState({
        //     showRestaurant: true,
        //     restaurantName: restaurant.name,
        //     restaurantDescription: restaurant.description,
        //     restaurantAddress: restaurant.address,
        //     restaurantPhoneNumber: restaurant.phoneNumber,
        //     restaurantFreeTables: restaurant.freeTables,
        //     restaurantTotalTables: restaurant.totalTables,
        //     restaurantType: this.renderType(restaurant.type)
        // });
    }

    renderType(value) {
        if (value == 0)
            return "Restaurant";
        if (value == 1)
            return "Pub";
        if (value == 2)
            return "Bar";
    }

    search(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].id === nameKey) {
                return myArray[i];
            }
        }
    }

    hideMarker() {
        this.setState({showRestaurant: false});
    }


    _filterData(query) {
        var data = this.state.restaurantsList;
        var restaurantFiltered = [];
        if (query !== '') {
            data.forEach(function (entry) {
                if (entry.name.includes(query) || entry.description.includes(query) || entry.address.city.includes(query))
                    restaurantFiltered.push(entry);
            });
        }


        return restaurantFiltered;
    }
    render() {

        const {region} = this.state;
        const {children, renderMarker} = this.props;
        const {query} = this.state;
        const data = this._filterData(query);
        return (
            <View style={styles.container}>
                <MapView
                    showsUserLocation
                    ref={map => {
                        this.map = map
                    }}
                    initialRegion={initialRegion}
                    renderMarker={renderMarker}
                    onMapReady={this.onMapReady}
                    showsMyLocationButton={false}
                    onRegionChange={this.onRegionChange}
                    onRegionChangeComplete={this.onRegionChangeComplete}
                    style={StyleSheet.absoluteFill}
                    textStyle={{color: '#bc8b00'}}
                    containerStyle={{backgroundColor: 'white', borderColor: '#BC8B00'}}>
                    {this.state.markers.map(marker => (
                        <Marker
                            onPress={() => this.showMarker(marker.key)}
                            coordinate={{longitude: marker.longitude, latitude: marker.latitude}}
                            title={marker.title}
                            description={marker.description}
                            key={marker.key}
                        />
                    ))}
                </MapView>



                <Icon name="map-search" color="#000"
                      raised
                      style={styles.iconSearch}
                      onPress={() => this.setState({showSearchBar: !this.state.showSearchBar})}

                />

                {this.state.showSearchBar &&

                <Autocomplete
                    containerStyle={{
                        marginTop: 53, width: 300, marginLeft: 70, opacity: 0.8,
                    }}
                    data={data}
                    defaultValue={query}
                    onChangeText={text => this.setState({query: text})}
                    renderItem={({item, i}) => (
                        <TouchableOpacity onPress={() => this.showRestaurantSearch(item)}
                                          style={{paddingLeft: 10, marginBottom: 12, backgroundColor: "#fff"}}
                        >
                            <Text style={{fontWeight: 'bold', fontSize: 20}}>{item.name}</Text>
                            <Text>{item.address.city}</Text>
                            <Text style={{color: "#65cf73"}}>Free Tables: {item.freeTables}</Text>
                            <Text>Total Tables: {item.totalTables}</Text>
                        </TouchableOpacity>
                    )}

                />
                }
                <Overlay isVisible={this.state.showRestaurant} onBackdropPress={this.hideMarker.bind(this)}>
                    <Text style={styles.Name}> {this.state.restaurantName}</Text>
                    <Text style={styles.Type}>Type: {this.state.restaurantType}</Text>
                    <Text style={styles.Description}>Description: {this.state.restaurantDescription}</Text>
                    <Text style={styles.freeTables}>FreeTables: {this.state.restaurantFreeTables}</Text>
                    <Text style={styles.totalTables}>Total Tables: {this.state.restaurantTotalTables}</Text>
                    <Text style={styles.Phone}>Phone: {this.state.restaurantPhoneNumber}</Text>
                    <Text style={styles.Counter}>Free Tables: {this.state.restaurantFreeTables}</Text>
                    <Text style={styles.Street}>Street:{this.state.restaurantAddress.street} </Text>
                    <Text style={styles.Number}>Number:{this.state.restaurantAddress.number} </Text>
                    <Text style={styles.City}>City:{this.state.restaurantAddress.city} </Text>

                    <Icon name="clock" color="#000"
                          raised
                          style={styles.icon}

                    />
                    <View style={styles.triangle2}>
                        <Triangle
                            width={90}
                            height={150}
                            color={'#000'}
                            direction={'right'}
                        />
                    </View>

                    <View style={styles.triangle}>
                        <Triangle
                            width={90}
                            height={150}
                            color={'#000'}
                            direction={'left'}
                        />
                    </View>

                </Overlay>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    header: {
        flex: .1,
        backgroundColor: '#FF3366',
        flexDirection: 'row'
    },
    subHeader: {
        flex: .05,
        backgroundColor: '#85b1f1',
        alignItems: 'center'
    },
    body: {
        flex: .9,
    },
    input: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        backgroundColor: '#FF3366',
    },
    button: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        backgroundColor: '#FF3366',
        paddingTop: 40
    },
    search: {
        position: 'absolute',
        zIndex: 1,
        marginTop: 50,
        backgroundColor: 'rgba(255, 255, 255, 1)',
    },

    Name: {
        position:'absolute',
        marginTop:10,
        fontWeight: 'bold',

        width:140,
        fontSize:30,
        color:'#cc0000',
        marginLeft:100,
        borderWidth:3,
        borderColor:'#cc0000',

    },
    Type: {
        position:'absolute',
        marginTop:100,
        fontWeight: 'bold',
        width:400,
        marginLeft:1,
        fontSize:20,
    },
    Description: {
        position:'absolute',
        marginTop:130,
        fontWeight: 'bold',
        width:400,
        marginLeft:1,
        fontSize:20,
    },
    freeTables: {
        position:'absolute',
        marginTop:160,
        fontWeight: 'bold',
        width:400,
        marginLeft:1,
        fontSize:20,
    },
    totalTables: {
        position:'absolute',
        marginTop:190,
        fontWeight: 'bold',
        width:400,
        marginLeft:1,
        color:'#000',
        fontSize:20,

    },

    Phone: {
        position:'absolute',
        marginTop:310,
        fontWeight: 'bold',
        width:500,
        marginLeft:1,
        fontSize:20,
    },
    Counter: {
        position:'absolute',
        marginTop:380,
        fontWeight: 'bold',
        width:270,
        color:'#cc0000',
        fontSize:35,
        alignItems: 'center',
        fontStyle: 'italic',
        marginLeft: 30,
        borderWidth:5,
        borderColor: '#fff',
        borderRadius:10,
    },
    icon: {
        color:'#cc0000',
        fontSize:100,
        marginTop: 535,
        marginLeft:105,
    },
    triangle: {
        position:'absolute',
        marginLeft:230,
        marginTop:535,

    },
    triangle2: {
        position:'absolute',
        marginLeft:10,
        marginTop:535,
    },
    iconSearch: {
        position:'absolute',
        marginTop:50,
        fontSize:40,
        marginLeft:25,
    },
    Street: {
        position:'absolute',
        marginTop:220,
        fontWeight: 'bold',
        width:400,
        marginLeft:1,
        fontSize:20,
    },
    Number: {
        position:'absolute',
        marginTop:250,
        fontWeight: 'bold',
        width:400,
        marginLeft:1,
        fontSize:20,
    },
    City: {
        position:'absolute',
        marginTop:280,
        fontWeight: 'bold',
        width:400,
        marginLeft:1,
        fontSize:20,
    }





});
