import React from 'react';
import {Text, View, ScrollView, Alert, Linking, StyleSheet,TouchableOpacity} from 'react-native';
import {Header, Input, SearchBar, Button} from 'react-native-elements';
import {Collapse, CollapseHeader, CollapseBody} from "accordion-collapse-react-native";
import {Spinner, ListItem, Separator} from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import NetInfo from "@react-native-community/netinfo";
import Triangle from 'react-native-triangle';

const AlertMsg = 'Are you sure you want to delete this entry?';
import {Font} from 'expo';
import * as SQLite from 'expo-sqlite';



export default class Home extends React.Component {
    handleGoToRegister() {
        this.props.navigation.navigate('RegisterScreen');
    }
    handleGoToLogin() {
        this.props.navigation.navigate('LoginScreen');
    }
    handleRestaurantLogin() {
        this.props.navigation.navigate('RestaurantLogin');
    }

    render() {
        return (
            <View style={styles.container}>


                <Icon name="food" color="#900"
                      raised
                      style={styles.header}

                />

                <Text style={styles.bottom}>
                    A.R.T.
                </Text>


                <Text style={styles.bottomSmall}>
                    Availabe Restaurant Tables
                </Text>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={styles.loginScreenButton}
                        onPress={() =>this.handleGoToRegister()}
                        underlayColor='#fff'>
                        <Text style={styles.loginText}>Register</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.loginScreenButton}
                        onPress={() =>this.handleGoToLogin()}
                        underlayColor='#fff'>
                        <Text style={styles.loginText}>Login</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.loginScreenButton}
                        onPress={() =>this.handleRestaurantLogin()}
                        underlayColor='#fff'>
                        <Text style={styles.loginText}>Restaurant</Text>
                    </TouchableOpacity>


                </View>



                <View style={styles.triangleContainer}>
                    <Triangle
                        width={90}
                        height={150}
                        color={'#000'}
                        direction={'right'}
                    />
                </View>


                <View style={styles.triangleContainerRight}>
                    <Triangle
                        width={90}
                        height={150}
                        color={'#cc0000'}
                        direction={'left'}
                    />
                </View>



            </View>


        );
    }

}
// #FFA90A
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 0,
        backgroundColor:'#fff',
        padding:50,

    },
    loginScreenButton:{
        marginTop:30,
        marginLeft:60,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#000',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor:'#fff',
        width: 200,
    },
    loginText:{
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize: 25,
        color: '#fff',
        fontWeight: 'bold',

    },
    triangleContainer:{
        width: '100%',
        height: 50,
        justifyContent: 'center',
        // alignItems: 'center',
        position: 'absolute', //Here is the trick
        bottom: 80, //Here is the trick
        marginLeft: 20
    },
    triangleContainerRight:{
        width: '100%',
        height: 50,
        justifyContent: 'center',
        position: 'absolute',
        bottom: 80,
        marginLeft: 300
    },
    header: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 120,
        textAlign: 'center',
        position: 'absolute',
        bottom: 60,
        left:140
    },
    buttonContainer:{
        marginTop: 200
    },
    registerButton: {
        color: '#006600',
    },

    bottom: {
        color:'#cc0000',
        fontSize: 70,
        marginLeft: 60,

    },
    bottomSmall: {
        marginRight: 300,
        marginLeft: -5,
        fontSize: 25,
        width:500,
        color: '#cc0000',
    }





});
// #737373

