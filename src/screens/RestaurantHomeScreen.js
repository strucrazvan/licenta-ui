import React from 'react';
import {ActivityIndicator,Alert, ScrollView, Button, TextInput, View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import {Header, Input, SearchBar,} from 'react-native-elements';
import {Collapse, CollapseHeader, CollapseBody} from "accordion-collapse-react-native";
import {Spinner, ListItem, Separator} from 'native-base';
import {Font} from 'expo';
import * as SQLite from 'expo-sqlite';
import NetInfo from "@react-native-community/netinfo";
import {parse, stringify} from "qs";
import * as axios from 'axios';
import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import DatePicker from 'react-native-datepicker'
import Triangle from 'react-native-triangle';
export default class RestaurantHomeScreen extends React.Component {
    constructor(props) {
        super(props);
        console.log("new screen \n\n\n");
        console.log(this.props.navigation.state.params);
    }
    componentDidMount() {
        this.setState({
            freeTables: this.props.navigation.state.params.restaurant.freeTables,
            totalTables: this.props.navigation.state.params.restaurant.totalTables
        })
    }
    state = {
        api: 'http://192.168.0.147:8000',
        freeTables: 0,
        totalTables: 0,
        loading:false
    };
    handleAdd() {
        var freeTables = this.state.freeTables;

        if (freeTables + 1 <= this.state.totalTables) {
            this.setState({
                loading:true
            })
            this.setNumberOfTablesApi(freeTables+1);
            this.setState({
                freeTables: freeTables + 1
            })
        } else {
            Alert.alert("Maximum number of tables is: " + this.state.totalTables);
        }
    }
    handleSubstract() {
        var freeTables = this.state.freeTables;
        if (freeTables - 1 >= 0) {
            this.setState({
                loading:true
            })
            this.setNumberOfTablesApi(freeTables-1);
            this.setState({
                freeTables: freeTables - 1
            })
        } else {
            Alert.alert("Cannot set the number of tables to negative");
        }

    }

    logoutHandler() {
        this.props.navigation.navigate('RestaurantLogin',{username: '', password: ''});
    }
    setNumberOfTablesApi(freeTables)
    {
        let data = new URLSearchParams();
        data.append("username", this.props.navigation.state.params.username);
        data.append("password", this.props.navigation.state.params.password);
        data.append("freeTables", freeTables);
        console.log(data);
        const url = this.state.api + '/restaurant/free-tables';
        axios.post(url, data)
            .then(
                res => {
                    console.log(res.data);
                    this.setState({
                        loading:false
                    })
                    // this.props.navigation.navigate('RestaurantHomeScreen',{username: username, password: password, restaurant: res.data});
                }
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                    else {
                        Alert.alert("Server error, please contact our team.");
                    }
                    this.setState({
                        loading:false
                    })
                }
            });
    }
    showContent(){
        return !this.state.loading
    }
    handleHomeButton() {
        this.props.navigation.navigate('Home');
    }
    render() {
        return (

            <View style={styles.container}>
                <TouchableOpacity  style={styles.logout}
                                   onPress={this.handleHomeButton.bind(this)}
                >
                    <Triangle
                        width={50}
                        height={50}
                        color={'#cc0000'}
                        direction={'left'}
                    />
                </TouchableOpacity >
                <Text style={styles.restaurantName}>
                    {this.props.navigation.state.params.restaurant.name}

                </Text>




                <TouchableOpacity
                    style={styles.loginScreenButton}
                    onPress={() =>this.handleAdd()}
                    underlayColor='#fff'>
                    <Text style={styles.loginText}>+</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.loginScreenButton2}
                    onPress={() =>this.handleSubstract()}
                    underlayColor='#fff'>
                    <Text style={styles.loginText}>-</Text>
                </TouchableOpacity>


                <View style={styles.countersContainer}>
                    <Text style={styles.freeTables}> Free tables: </Text>
                    <Text style={styles.freeTables}>
                        {this.state.freeTables}
                    </Text>
                </View>



                <View style={styles.countersContainer}>
                    <Text style={styles.totalTables}> Total tables: </Text>
                    <Text style={styles.totalTables}>
                        {this.state.totalTables}
                    </Text>

                </View>












                {this.state.loading &&
                <ActivityIndicator size="large" color="#0000ff" />
                }








            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        zIndex:0,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    input: {
        width: 200,
        height: 33,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor:'#000',
    },
    header: {
        color: '#000',
        fontWeight: 'bold',

    },
    loginScreenButton:{
        marginTop:5,
        marginLeft:0,
        paddingTop:10,
        paddingBottom:0,
        backgroundColor:'#009900',
        alignItems: 'center',
        borderRadius: 5,
        borderWidth: 0,
        borderColor:'#009900',
        height: 300,
        width: 300,
    },
    loginScreenButton2: {

        marginTop:10,
        marginLeft:0,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#b30000',
        alignItems: 'center',
        borderRadius: 5,
        borderWidth: 0,
        borderColor:'#0dff00',
        height: 300,
        width: 300,
    },
    loginText:{
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize: 200,
        color: '#fff',
        fontWeight: 'bold',
        marginBottom:800

    },
    radioButton :{
        color: '#000',
    },
    icon: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 75,

        position: 'absolute',
        bottom: 800,
        left:5
    },
    loading: {
        zIndex:1,
        position: 'absolute',
        textAlign:'center',
        paddingTop:350,


    },
    loadingText:{
        fontWeight:'bold',
        marginBottom:1100,
        paddingLeft:320,
        fontSize:20
    },
    restaurantName: {
        fontSize:50,
        textAlign: 'center',
        borderWidth:5,
        borderRadius: 10,
        borderWidth: 5,
        borderColor: '#000',
        marginBottom:-3

    },
    totalTables: {
        color:'#b30000',
        fontSize:50,
        fontWeight:'bold'

    },
    freeTables: {
        color:'#009900',
        fontSize:50,
        fontWeight:'bold',
        marginTop:19
    },
    countersContainer:{
        flexDirection: 'row',
        textAlign:'center'

    },
    logout:{
        left:0,
        top:10,
        position: 'absolute'
    },
});
