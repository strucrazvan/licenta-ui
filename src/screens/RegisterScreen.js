import React from 'react';
import {Alert, Button, TextInput, View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import {Header, Input, SearchBar,} from 'react-native-elements';
import {Collapse, CollapseHeader, CollapseBody} from "accordion-collapse-react-native";
import {Spinner, ListItem, Separator} from 'native-base';
import {Font} from 'expo';
import * as SQLite from 'expo-sqlite';
import NetInfo from "@react-native-community/netinfo";
import {stringify} from "qs";
import * as axios from 'axios';
import Triangle from 'react-native-triangle';

var radio_props = [
    {label: 'User', value: 1},
    {label: 'Owner', value: 0}
];

export default class RegisterScreen extends React.Component {
    state = {
        api: 'http://192.168.0.147:8000',
        username: '',
        email: '',
        emailError: '',
        password: '',
        repeatPassword: '',
        passwordError: '',
        firstName: '',
        lastName: '',
        userType: 1,
    };
    registerUser(username, email, password, firstName, lastName) {
        console.log(this.state.api);
        console.log(this.state.api + '/user/create');

        let data = new URLSearchParams();
        data.append("user[username]", username);
        data.append("user[email]", email);
        data.append("user[firstName]", firstName);
        data.append("user[lastName]", lastName);
        data.append("user[password]", password);
        data.append("user[save]", "");

        const url = this.state.api + '/user/create';
        axios.post(url, data)
            .then(
                res => Alert.alert("User created"),
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                } else {
                    Alert.alert("Server error, please contact our team.");
                }

            });
    }

    registerOwner(username, email, password, firstName, lastName) {
        console.log(this.state.api);
        console.log(this.state.api + '/user/create');

        let data = new URLSearchParams();
        data.append("owner[username]", username);
        data.append("owner[email]", email);
        data.append("owner[firstName]", firstName);
        data.append("owner[lastName]", lastName);
        data.append("owner[password]", password);
        data.append("owner[save]", "");

        const url = this.state.api + '/owner/create';
        axios.post(url, data)
            .then(
                res => Alert.alert("User created")
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                } else {
                    Alert.alert("Server error, please contact our team.");
                }

            });
    }

    registerHandler() {
        const {api, username, email, emailError, password, repeatPassword, firstName, lastName, userType} = this.state;
        if (username === '' || email === '' || firstName === '' || lastName === '' || password === '' || repeatPassword === '') {
            Alert.alert("Please complete all the fields");
        } else if (userType === 1) {
            this.registerUser(username, email, password, firstName, lastName);
        } else if (userType === 0) {
            this.registerOwner(username, email, password, firstName, lastName);
        }
    }

    validateEmail = (text) => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            this.setState({
                emailError: 'Email is not valid!'
            });
            this.setState({email: text})
            return false;
        } else {
            this.setState({email: text})
            this.setState({
                emailError: ''
            });
            console.log("Email is Correct");
        }
    }
    validatePassword = (text) => {
        let password = this.state.password;
        if (text !== password) {
            this.setState({
                emailError: 'Password does not match!'
            });
            this.setState({repeatPassword: text})
            return false;
        } else {
            this.setState({repeatPassword: text})
            this.setState({
                emailError: ''
            });
        }
    }
    handleHomeButton() {
        this.props.navigation.navigate('Home');
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.header}>
                    Register
                </Text>
                <Text>{this.state.emailError}</Text>

                <TextInput
                    onChangeText={(val) => this.setState({firstName: val})} value={this.state.firstName}
                    placeholder={'First Name'}
                    style={styles.input}
                />
                <TextInput
                    onChangeText={(val) => this.setState({lastName: val})} value={this.state.lastName}
                    placeholder={'Last Name'}
                    style={styles.input}
                />

                <TextInput
                    onChangeText={(val) => this.setState({username: val})} value={this.state.username}

                    placeholder={'Username'}
                    style={styles.input}
                />
                <TextInput
                    onChangeText={(val) => this.setState({password: val})} value={this.state.password}
                    placeholder={'Password'}
                    secureTextEntry={true}
                    style={styles.input}
                />
                <TextInput
                    onChangeText={(val) => this.validatePassword(val)} value={this.state.repeatPassword}

                    placeholder={'Password'}
                    secureTextEntry={true}
                    style={styles.input}
                />
                <TextInput
                    placeholder="Email"
                    onChangeText={(text) => this.validateEmail(text)}
                    value={this.state.email}
                    style={styles.input}
                />
                <RadioForm
                    radio_props={radio_props}
                    initial={0}
                    onPress={(value) => {
                        this.setState({userType: value})
                    }}
                    buttonColor={'#000'}
                    labelColor={'#000'}
                />


                <TouchableOpacity
                    style={styles.loginScreenButton}
                    onPress={() =>this.registerHandler()}
                    underlayColor='#fff'>
                    <Text style={styles.loginText}>Register</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.triangleContainerRight}
                                   onPress={this.handleHomeButton.bind(this)}
                >
                    <Triangle
                        width={50}
                        height={50}
                        color={'#cc0000'}
                        direction={'left'}
                    />
                </TouchableOpacity >


            </View>
        );

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    input: {
        width: 200,
        height: 44,
        padding: 10,
        marginBottom: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor:'#000'
    },
    header: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 60,
    },
    triangleContainerRight:{
        position: 'absolute',
        top: 40,
        left:10
    },
    loginScreenButton:{
        marginTop:10,
        marginLeft:0,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#000',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 0,
        borderColor:'#fff',
        width: 100,
    },
    loginText:{
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold',

    },
    radioButton :{
        color: '#000',
    }
});
