import React from 'react';
import {Alert, ScrollView, Button, TextInput, View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import {Header, Input, SearchBar,} from 'react-native-elements';
import {Collapse, CollapseHeader, CollapseBody} from "accordion-collapse-react-native";
import {Spinner, ListItem, Separator} from 'native-base';
import {Font} from 'expo';
import * as SQLite from 'expo-sqlite';
import NetInfo from "@react-native-community/netinfo";
import {parse, stringify} from "qs";
import * as axios from 'axios';
import RNPickerSelect from 'react-native-picker-select';
import IconF from 'react-native-vector-icons/FontAwesome';
import DatePicker from 'react-native-datepicker'

const db = SQLite.openDatabase('shop.db')
var radio_props = [
    {label: 'User', value: 1},
    {label: 'Owner', value: 0}
];

export default class OwnerHomeScreen extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        console.log(props.username);
        console.log(props.state);
    }

    componentDidMount() {
        this.getRestaurants();
        this.getOwner();
    }

    state = {
        api: 'http://192.168.0.147:8000',
        showCreateRestaurant: false,
        restaurantName: '',
        restaurantDescription: '',
        restaurantTotalTables: '',
        restaurantType: '',
        restaurantPhoneNumber: '',
        restaurantUserName: '',
        restaurantPassword: '',
        restaurantCity: '',
        restaurantCountry: '',
        restaurantProvince: '',
        restaurantStreet: '',
        restaurantNumber: '',
        restaurantPostalCode: '',
        editId: null,
        restaurantsList: [],
        showEditUser: false,
        owner: [],
        emailError: '',
        firstName: "",
        lastName: '',
        password: '',
        repeatPassword: '',
        userCountry: '',
        userProvince: '',
        userCity: '',
        userStreet: '',
        userNumber: '',
        userPostalCode: '',
        userBirthDate: ''
    };

    getOwner() {
        let data = new URLSearchParams();

        data.append("username", this.props.navigation.state.params.username);
        data.append("password", this.props.navigation.state.params.password);
        axios.post(this.state.api + '/owner/get', data)
            .then(
                res => {
                    console.log("OWNER\n\n");
                    console.log(res);
                    this.setState({
                        owner: res.data,
                        firstName: res.data.firstName,
                        lastName: res.data.lastName,
                        userBirthDate: res.data.birthDate
                    })
                    //todo update COuntry picker and birthdate
                    if (res.data.address) {
                        this.setState({
                            userCountry: res.data.address.country,
                            userProvince: res.data.address.province,
                            userCity: res.data.address.city,
                            userStreet: res.data.address.street,
                            userNumber: res.data.address.number,
                            userPostalCode: res.data.address.postalCode,
                        })
                    }
                }
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                } else {
                    Alert.alert("Server error, please contact our team.");
                }

            });
    }

    // this.getData();
    // // fetch(this.state.api + '/user/create' , {
    // //     method: 'GET'
    // // })
    // //     .then((response) => response.json())
    // //     .then((responseJson) => {
    // //         // this.setState({
    // //         //     data: responseJson,
    // //         // })
    // //         console.log(responseJson);
    // //     })
    // //     .catch((error) => {
    // //         console.log("GEt");
    // //         console.log(error)
    // //
    // //     });
    registerUser(username, email, password, firstName, lastName) {

        let data = new URLSearchParams();
        data.append("user[username]", username);
        data.append("user[email]", email);
        data.append("user[firstName]", firstName);
        data.append("user[lastName]", lastName);
        data.append("user[password]", password);
        data.append("user[save]", "");

        const url = this.state.api + '/user/create';
        axios.post(url, data)
            .then(
                res => Alert.alert("User created"),
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                } else {
                    Alert.alert("Server error, please contact our team.");
                }

            });
    }

    createRestaurantApi(
        restaurantName,
        restaurantDescription,
        restaurantTotalTables,
        restaurantType,
        restaurantPhoneNumber,
        restaurantUserName,
        restaurantPassword,
        restaurantCity,
        restaurantCountry,
        restaurantProvince,
        restaurantStreet,
        restaurantNumber,
        restaurantPostalCode,
        restaurantOwnerUsername,
        restaurantOwnerPassword) {

        let data = new URLSearchParams();
        data.append("restaurant[name]", restaurantName);
        data.append("restaurant[description]", restaurantDescription);
        data.append("restaurant[type]", restaurantType);
        data.append("restaurant[totalTables]", restaurantTotalTables);
        data.append("restaurant[freeTables]", restaurantTotalTables);
        data.append("restaurant[address][country]", restaurantCountry);
        data.append("restaurant[address][city]", restaurantCity);
        data.append("restaurant[address][province]", restaurantProvince);
        data.append("restaurant[address][street]", restaurantStreet);
        data.append("restaurant[address][number]", restaurantNumber);
        data.append("restaurant[address][postalCode]", restaurantPostalCode);
        data.append("restaurant[phoneNumber]", restaurantPhoneNumber);
        data.append("restaurant[username]", restaurantUserName);
        data.append("restaurant[password]", restaurantPassword);
        data.append("username", restaurantOwnerUsername);
        data.append("password", restaurantOwnerPassword);
        data.append("restaurant[save]", "");

        axios.post(this.state.api + '/restaurant/create', data)
            .then(
                res => {
                    Alert.alert("Restaurant created")
                    var showCreateRestaurant = this.state.showCreateRestaurant;
                    this.setState({showCreateRestaurant: !showCreateRestaurant})

                    this.getRestaurants();
                }
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                } else {
                    Alert.alert("Server error, please contact our team.");
                }

            });
    }

    editRestaurantApi(
        restaurantId,
        restaurantName,
        restaurantDescription,
        restaurantTotalTables,
        restaurantType,
        restaurantPhoneNumber,
        restaurantUserName,
        restaurantPassword,
        restaurantCity,
        restaurantCountry,
        restaurantProvince,
        restaurantStreet,
        restaurantNumber,
        restaurantPostalCode,
        restaurantOwnerUsername,
        restaurantOwnerPassword) {

        let data = new URLSearchParams();
        data.append("restaurant[name]", restaurantName);
        data.append("restaurant[description]", restaurantDescription);
        data.append("restaurant[type]", restaurantType);
        data.append("restaurant[totalTables]", restaurantTotalTables);
        data.append("restaurant[freeTables]", restaurantTotalTables);
        data.append("restaurant[address][country]", restaurantCountry);
        data.append("restaurant[address][city]", restaurantCity);
        data.append("restaurant[address][province]", restaurantProvince);
        data.append("restaurant[address][street]", restaurantStreet);
        data.append("restaurant[address][number]", restaurantNumber);
        data.append("restaurant[address][postalCode]", restaurantPostalCode);
        data.append("restaurant[phoneNumber]", restaurantPhoneNumber);
        data.append("restaurant[username]", restaurantUserName);
        data.append("restaurant[password]", restaurantPassword);
        data.append("username", restaurantOwnerUsername);
        data.append("password", restaurantOwnerPassword);
        data.append("restaurant[save]", "");

        axios.post(this.state.api + '/restaurant/edit/' + restaurantId, data)
            .then(
                res => {
                    Alert.alert("Restaurant modified")
                    var showCreateRestaurant = this.state.showCreateRestaurant;
                    this.setState({showCreateRestaurant: !showCreateRestaurant})

                    this.getRestaurants();
                }
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                } else {
                    Alert.alert("Server error, please contact our team.");
                }

            });
    }

    getRestaurants() {
        let data = new URLSearchParams();

        data.append("username", this.props.navigation.state.params.username);
        data.append("password", this.props.navigation.state.params.password);
        axios.post(this.state.api + '/restaurant/get-restaurants-by-owner', data)
            .then(
                res => {
                    this.setState({restaurantsList: res.data})
                }
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                } else {
                    Alert.alert("Server error, please contact our team.");
                }

            });
    }

    editHandler() {

    }

    logoutHandler() {
        this.props.navigation.navigate('LoginScreen', {username: '', password: ''});
    }

    createRestaurant() {
        // console.log("RESTAURANTS:    ");
        // this.getRestaurants();
        var showCreateRestaurant = this.state.showCreateRestaurant;
        this.setState({showCreateRestaurant: !showCreateRestaurant});
        this.restartRestaurant();
    }

    editUser() {
        // console.log("RESTAURANTS:    ");
        // this.getRestaurants();
        var showEditUser = this.state.showEditUser;
        this.setState({showEditUser: !showEditUser});
    }

    restartRestaurant() {
        this.setState({
            restaurantName: '',
            restaurantDescription: '',
            restaurantTotalTables: '',
            restaurantType: '',
            restaurantPhoneNumber: '',
            restaurantUserName: '',
            restaurantPassword: '',
            restaurantCity: '',
            restaurantCountry: '',
            restaurantProvince: '',
            restaurantStreet: '',
            restaurantNumber: '',
            restaurantPostalCode: '',
            editId: null,
        })
    }

    onRestaurantSubmit() {
        const {
            api,
            showCreateRestaurant,
            restaurantName,
            restaurantDescription,
            restaurantTotalTables,
            restaurantType,
            restaurantPhoneNumber,
            restaurantUserName,
            restaurantPassword,
            restaurantCity,
            restaurantCountry,
            restaurantProvince,
            restaurantStreet,
            restaurantNumber,
            restaurantPostalCode,
            restaurantsList,
            editId
        } = this.state;
        var restaurantOwnerUsername = this.props.navigation.state.params.username,
            restaurantOwnerPassword = this.props.navigation.state.params.password;
        //TODO EMPTY VALIDATION

        if (editId !== null) {
            this.editRestaurantApi(editId, restaurantName,
                restaurantDescription,
                restaurantTotalTables,
                restaurantType,
                restaurantPhoneNumber,
                restaurantUserName,
                restaurantPassword,
                restaurantCity,
                restaurantCountry,
                restaurantProvince,
                restaurantStreet,
                restaurantNumber,
                restaurantPostalCode,
                restaurantOwnerUsername,
                restaurantOwnerPassword);
        } else {
            this.createRestaurantApi(restaurantName,
                restaurantDescription,
                restaurantTotalTables,
                restaurantType,
                restaurantPhoneNumber,
                restaurantUserName,
                restaurantPassword,
                restaurantCity,
                restaurantCountry,
                restaurantProvince,
                restaurantStreet,
                restaurantNumber,
                restaurantPostalCode,
                restaurantOwnerUsername,
                restaurantOwnerPassword);
        }
    }

    renderType(value) {
        if (value == 0)
            return "Restaurant";
        if (value == 1)
            return "Pub";
        if (value == 2)
            return "Bar";
    }

    handleEditRestaurant(restaurantId) {
        console.log(restaurantId);
        let item = this.search(restaurantId, this.state.restaurantsList);

        this.setState({
            restaurantName: item.name,
            restaurantDescription: item.description,
            restaurantTotalTables: item.totalTables.toString(),
            restaurantType: item.type.toString(),
            restaurantPhoneNumber: item.phoneNumber,
            restaurantUserName: item.username,
            restaurantPassword: item.password,
            restaurantCity: item.address.city,
            restaurantCountry: item.address.country,
            restaurantProvince: item.address.province,
            restaurantStreet: item.address.street,
            restaurantNumber: item.address.number,
            restaurantPostalCode: item.address.postalCode,
            editId: item.id,
        })
        this.setState({showCreateRestaurant: true});
    }

    search(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].id === nameKey) {
                return myArray[i];
            }
        }
    }

    closeForm() {
        this.setState({showCreateRestaurant: false});
        this.setState({showEditUser: false});
        this.restartRestaurant();
    }

    onOwnerSubmit() {
        var {
            api,
            showCreateRestaurant,
            restaurantName,
            restaurantDescription,
            restaurantTotalTables,
            restaurantType,
            restaurantPhoneNumber,
            restaurantUserName,
            restaurantPassword,
            restaurantCity,
            restaurantCountry,
            restaurantProvince,
            restaurantStreet,
            restaurantNumber,
            restaurantPostalCode,
            editId,
            restaurantsList,
            showEditUser,
            owner,
            emailError,
            firstName,
            lastName,
            password,
            repeatPassword,
            userCountry,
            userProvince,
            userCity,
            userStreet,
            userNumber,
            userPostalCode,
            userBirthDate
        } = this.state;
        var restaurantOwnerUsername = this.props.navigation.state.params.username,
            restaurantOwnerPassword = this.props.navigation.state.params.password;
        //TODO VALIDATION
        if (password === '') {
            password = restaurantOwnerPassword
        }
        this.editOwnerApi(
            this.state.owner.id,
            restaurantOwnerUsername,
            this.state.owner.email,
            password,
            firstName,
            lastName,
            userCity,
            userCountry,
            userProvince,
            userStreet,
            userNumber,
            userPostalCode,
            userBirthDate,
            restaurantOwnerPassword)
    }


    editOwnerApi(
        ownerId,
        username,
        email,
        newPassword,
        firstName,
        lastName,
        userCity,
        userCountry,
        userProvince,
        userStreet,
        userNumber,
        userPostalCode,
        userBirthDate,
        oldPassword) {
        var date = userBirthDate.split('-');
        let data = new URLSearchParams();
        data.append("owner[username]", username);
        data.append("owner[email]", email);
        data.append("owner[firstName]", firstName);
        data.append("owner[lastName]", lastName);
        data.append("owner[password]", newPassword);
        data.append("owner[address][country]", userCountry);
        data.append("owner[address][city]", userCity);
        data.append("owner[address][province]", userProvince);
        data.append("owner[address][street]", userStreet);
        data.append("owner[address][number]", userNumber);
        data.append("owner[address][postalCode]", userPostalCode);
        data.append("owner[birthDate][month]", date[1]);
        data.append("owner[birthDate][year]", date[0]);
        data.append("owner[birthDate][day]", date[2]);

        data.append("username", username);
        data.append("password", oldPassword);

        data.append("owner[save]", "");
        console.log(data);
        axios.post(this.state.api + '/owner/edit/' + ownerId, data)
            .then(
                res => {
                    Alert.alert("Owner saved")
                    var showEditUser = this.state.showEditUser;
                    this.setState({showEditUser: !showEditUser})

                    this.getOwner();
                }
            )
            .catch(error => {
                if (error.response) {
                    if (error.response.status === 409) {
                        Alert.alert(error.response.data)
                    }
                } else {
                    Alert.alert("Server error, please contact our team.");
                }

            });
    }

    validatePassword = (text) => {
        let password = this.state.password;
        if (text !== password) {
            this.setState({
                emailError: 'Password does not match!'
            });
            this.setState({repeatPassword: text})
            return false;
        } else {
            this.setState({repeatPassword: text})
            this.setState({
                emailError: ''
            });
        }
    }

    render() {


        const listItem = this.state.restaurantsList.map((item) =>
            <View key={item.id}>
                <Collapse style={{marginBottom: 10, marginTop: 10}}>
                    <CollapseHeader>
                        <Separator bordered>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text style={{fontSize: 20, color: '#000000',}}>{item.name}</Text>
                                <Text
                                    style={{fontSize: 20, color: '#cc0000', fontWeight: 'bold'}}>{item.freeTables}/{item.totalTables}</Text>
                                <View style={{flexDirection: 'row',}}>

                                    <TouchableOpacity
                                        style={{marginRight: 20,padding:4,borderRadius:10, backgroundColor: '#afafaf'}}
                                        onPress={() => this.handleEditRestaurant(item.id)}
                                        underlayColor='#fff'>
                                        <Text style={styles.editButton}>Edit</Text>
                                    </TouchableOpacity>


                                </View>
                            </View>
                        </Separator>
                    </CollapseHeader>
                    <CollapseBody>

                        <ListItem style={{marginBottom: 3, marginTop: 3}}>
                            <Text><Text> Id : </Text>{item.id}</Text>
                        </ListItem>
                        <ListItem style={{marginBottom: 3, marginTop: 3,}}>
                            <Text><Text> Name : </Text>{item.name}</Text>
                        </ListItem>
                        <ListItem style={{marginBottom: 3, marginTop: 3}}>
                            <Text><Text> Type : </Text>{this.renderType(item.type)}</Text>
                        </ListItem>
                        <ListItem style={{marginBottom: 3, marginTop: 3}}>
                            <Text><Text> Free Tables: </Text>{item.freeTables}</Text>
                        </ListItem>
                        <ListItem style={{marginBottom: 3, marginTop: 3}}>
                            <Text><Text> Total Tables: </Text>{item.totalTables}</Text>
                        </ListItem>
                        <ListItem style={{marginBottom: 3, marginTop: 3}}>
                            <Text><Text> Username: </Text>{item.username}</Text>
                        </ListItem>
                    </CollapseBody>
                </Collapse>
            </View>
        );
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={{
                        paddingTop: 0,
                        fontSize: 20,
                        fontWeight: 'bold'
                    }}>{this.props.navigation.state.params.username}</Text>
                    <Text style={{
                        marginLeft: 50, paddingTop: 0, fontSize: 20, borderRadius: 10,
                        borderWidth: 2
                    }}
                          onPress={this.editUser.bind(this)}> Edit</Text>
                    <Text style={{
                        marginLeft: 95, paddingTop: 0, fontSize: 20, marginBottom: 10,
                        borderRadius: 10,
                        borderWidth: 2
                    }}
                          onPress={this.logoutHandler.bind(this)}>Logout</Text>
                </View>
                <View style={styles.subHeader} onTouchStart={this.createRestaurant.bind(this)}>
                    <Text style={{
                        fontSize: 20, borderRadius: 10,
                        borderWidth: 2, marginRight: 10
                    }}>Create Restaurant</Text>
                </View>


                <ScrollView style={[styles.body]}>
                    {this.state.showCreateRestaurant &&
                    <View>
                        <Text style={styles.allButtonText}>Name</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantName: val})}
                            value={this.state.restaurantName}
                            placeholder={'Restaurant Name'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Description</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantDescription: val})}
                            value={this.state.restaurantDescription}
                            placeholder={'Restaurant Description'}
                            style={styles.input}
                        />
                        <Text style={styles.allButtonText}>Type</Text>
                        <RNPickerSelect
                            onValueChange={(value) => this.setState({restaurantType: value})}
                            items={[
                                {label: 'Restaurant', value: '0'},
                                {label: 'Pub', value: '1'},
                                {label: 'Bar', value: '2'},
                            ]}
                        />
                        <Text style={styles.allButtonText}>Country</Text>
                        <RNPickerSelect
                            onValueChange={(value) => this.setState({restaurantCountry: value})}
                            items={[
                                {label: 'Romania', value: 'Romania'},
                                {label: 'Hungary', value: 'Hungary'},
                                {label: 'Canada', value: 'Canada'},
                            ]}
                        />
                        <Text style={styles.allButtonText}>Province</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantProvince: val})}
                            value={this.state.restaurantProvince}
                            placeholder={'Restaurant Province'}
                            style={styles.input}
                        />
                        <Text style={styles.allButtonText}>City</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantCity: val})}
                            value={this.state.restaurantCity}
                            placeholder={'Restaurant City'}
                            style={styles.input}
                        />
                        <Text style={styles.allButtonText}>Street</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantStreet: val})}
                            value={this.state.restaurantStreet}
                            placeholder={'Restaurant Street'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Street Number</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantNumber: val})}
                            value={this.state.restaurantNumber}
                            placeholder={'Restaurant Number'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Postal Code</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantPostalCode: val})}
                            value={this.state.restaurantPostalCode}
                            placeholder={'Restaurant Postal Code'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Phone Number</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantPhoneNumber: val})}
                            value={this.state.restaurantPhoneNumber}
                            placeholder={'Restaurant Phone Number'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Total Tables</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantTotalTables: val})}
                            value={this.state.restaurantTotalTables}
                            placeholder={'Restaurant Total Tables'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Username</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantUserName: val})}
                            value={this.state.restaurantUserName}
                            placeholder={'Restaurant Username'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Password</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({restaurantPassword: val})}
                            value={this.state.restaurantPassword}
                            secureTextEntry={true}
                            placeholder={'Restaurant Password'}
                            style={styles.input}
                        />


                        <TouchableOpacity
                            style={styles.button}
                            onPress={this.closeForm.bind(this)}
                            underlayColor='#fff'>
                            <Text style={styles.touchableOpacityText2}>CANCEL</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.button}
                            onPress={this.onRestaurantSubmit.bind(this)}
                            underlayColor='#fff'>
                            <Text style={styles.touchableOpacityText}>SAVE</Text>
                        </TouchableOpacity>
                    </View>
                    }


                    {this.state.showEditUser &&
                    <View>
                        <Text>{this.state.emailError}</Text>
                        <Text style={styles.longButtonText}>First name</Text>

                        <TextInput
                            onChangeText={(val) => this.setState({firstName: val})} value={this.state.firstName}
                            placeholder={'First Name'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Last name</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({lastName: val})} value={this.state.lastName}
                            placeholder={'Last Name'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Birth date</Text>

                        <DatePicker
                            style={{width: 200}}
                            date={this.state.userBirthDate}
                            mode="date"
                            placeholder="select date"
                            format="YYYY-MM-DD"
                            minDate="1990-01-01"
                            maxDate="2003-01-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 160,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    position: 'absolute',
                                    left: 196,
                                    height: 30

                                }
                            }}
                            onDateChange={(date) => {
                                this.setState({userBirthDate: date});
                            }}
                        />
                        <TextInput
                            onChangeText={(val) => this.setState({password: val})} value={this.state.password}
                            placeholder={'Password'}
                            secureTextEntry={true}
                            style={styles.input}
                        />
                        <TextInput
                            onChangeText={(val) => this.validatePassword(val)} value={this.state.repeatPassword}
                            placeholder={'Password'}
                            secureTextEntry={true}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Country</Text>
                        <RNPickerSelect
                            onValueChange={(value) => this.setState({userCountry: value})}
                            items={[
                                {label: 'Romania', value: 'Romania'},
                                {label: 'Hungary', value: 'Hungary'},
                                {label: 'Canada', value: 'Canada'},
                            ]}
                        />
                        <Text style={styles.longButtonText}>Province</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({userProvince: val})}
                            value={this.state.userProvince}
                            placeholder={'User Province'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>City</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({userCity: val})}
                            value={this.state.userCity}
                            placeholder={'User City'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Street</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({userStreet: val})}
                            value={this.state.userStreet}
                            placeholder={'User Street'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Street Number</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({userNumber: val})}
                            value={this.state.userNumber}
                            placeholder={'User Number'}
                            style={styles.input}
                        />
                        <Text style={styles.longButtonText}>Postal Code</Text>
                        <TextInput
                            onChangeText={(val) => this.setState({userPostalCode: val})}
                            value={this.state.userPostalCode}
                            placeholder={'User Postal Code'}
                            style={styles.input}
                        />

                        <TouchableOpacity
                            style={styles.button}
                            onPress={this.closeForm.bind(this)}
                            underlayColor='#fff'>
                            <Text style={styles.touchableOpacityText2}>CANCEL</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.button}
                            onPress={this.onRestaurantSubmit.bind(this)}
                            underlayColor='#fff'>
                            <Text style={styles.touchableOpacityText}>SAVE</Text>
                        </TouchableOpacity>


                    </View>
                    }
                    <View>
                        {listItem}
                    </View>
                </ScrollView>
            </View>
        );

    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 0,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 0,
        borderRadius: 10,
        marginTop: 30,

    },
    header: {
        flex: .1,
        backgroundColor: '#fff',
        flexDirection: 'row',
        borderWidth: 0,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 10,
        alignItems: 'center',


    },
    subHeader: {
        flex: 0,
        backgroundColor: '#fff',
        alignItems: 'center',


    },
    body: {
        flex: 0,
        backgroundColor: '#fff',
        marginTop: 50,


    },
    input: {
        width: 200,
        height: 40,
        padding: 5,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        backgroundColor: '#cc0000',
        borderRadius: 15,
        marginLeft: 110,

    },
    button: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 2,
        borderColor: '#cc0000',
        marginBottom: 0,
        backgroundColor: '#fff',
        paddingTop: 0,
        color: '#fff',
        borderRadius: 10,
        marginLeft: 110
    },
    allButtonText: {
        fontSize: 15,
        justifyContent: 'center',
        marginLeft: 180,
        fontWeight: 'bold',

    },
    longButtonText: {
        fontSize: 15,
        justifyContent: 'center',
        fontWeight: 'bold',
        marginLeft: 170

    },
    touchableOpacityText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginLeft: 50,
        marginBottom: 10

    },
    editButton: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    buttonSonia: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 2,
        borderColor: '#cc0000',
        marginBottom: 0,
        backgroundColor: '#fff',
        paddingTop: 0,
        color: '#fff',
        borderRadius: 10,
        marginLeft: 110
    },
    touchableOpacityText2: {
        fontSize: 25,
        fontWeight: 'bold',
        marginLeft: 35,
        marginBottom: 10
    }

});