import React, { Component } from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import Home from './screens/Home';
import RegisterScreen from "./screens/RegisterScreen";
import LoginScreen from "./screens/LoginScreen";
import OwnerHomeScreen from "./screens/OwnerHomeScreen";
import UserHomeScreen from "./screens/UserHomeScreen";
import RestaurantLogin from "./screens/RestaurantLogin";
import RestaurantHomeScreen from "./screens/RestaurantHomeScreen";

export default class Index extends Component {
    render() {
        return (    
            <AppContainer />
        );
    }
}

const AppSwitchNavigator = createSwitchNavigator({
    Home: { screen: Home },
    RegisterScreen: { screen: RegisterScreen},
    LoginScreen: { screen: LoginScreen},
    OwnerHomeScreen: { screen: OwnerHomeScreen},
    UserHomeScreen: { screen: UserHomeScreen},
    RestaurantLogin: { screen: RestaurantLogin},
    RestaurantHomeScreen: { screen: RestaurantHomeScreen},
});


const AppContainer = createAppContainer(AppSwitchNavigator);
